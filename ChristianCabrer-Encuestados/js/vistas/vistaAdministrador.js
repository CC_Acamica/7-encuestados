/*
 * Vista administrador
 */
var VistaAdministrador = function(modelo, controlador, elementos) {
    this.modelo = modelo;
    this.controlador = controlador;
    this.elementos = elementos;
    var contexto = this;

    // suscripción de observadores
    this.modelo.preguntaAgregada.suscribir(function() {
        contexto.reconstruirLista();
    });
    this.modelo.preguntaBorrada.suscribir(function() {
        contexto.reconstruirLista();
    });
    this.modelo.preguntaModificada.suscribir(function() {
        contexto.reconstruirLista();
    });
};


VistaAdministrador.prototype = {
    //lista
    inicializar: function() {
        //llamar a los metodos para reconstruir la lista, configurar botones y validar formularios
        validacionDeFormulario();
        this.reconstruirLista();
        this.configuracionDeBotones();
    },

    construirElementoPregunta: function(pregunta) {
        var contexto = this;
        var nuevoItem;
        //completar
        //asignar a nuevoitem un elemento li con clase "list-group-item", id "pregunta.id" y texto "pregunta.textoPregunta"
        nuevoItem = $('<li>', {
            class: "list-group-item",
            id: pregunta.id,
            text: pregunta.textoPregunta
        });
        var interiorItem = $('.d-flex');
        var titulo = interiorItem.find('h5');
        titulo.text(pregunta.textoPregunta);
        interiorItem.find('small').text(pregunta.cantidadPorRespuesta.map(function(resp) {
            return " " + resp.textoRespuesta;
        }));
        nuevoItem.html($('.d-flex').html());
        return nuevoItem;
    },

    reconstruirLista: function() {
        var lista = this.elementos.lista;
        lista.html('');
        var preguntas = this.modelo.preguntas;
        for (var i = 0; i < preguntas.length; ++i) {
            lista.append(this.construirElementoPregunta(preguntas[i]));
        }
    },

    configuracionDeBotones: function() {
        var e = this.elementos;
        var contexto = this;

        //asociacion de eventos a boton
        e.botonAgregarPregunta.click(function() {
            var value = e.pregunta.val();
            var respuestas = [];
            $('[name="option[]"]').each(function() {
                if ($(this).val() != "") {
                    respuestas.push({ 'textoRespuesta': $(this).val(), 'cantidad': 0, votantes: [] });
                }
            });
            try {
                contexto.controlador.agregarPregunta(value, respuestas);
                contexto.mostrarMensaje("Exitoso", "La pregunta fue agregada correctamente", "success");
                contexto.limpiarFormulario();
            } catch (Mensaje) {
                contexto.mostrarMensaje("Error", Mensaje, "error");
            }
        });
        e.botonBorrarPregunta.click(function() {
            var id = parseInt($('.list-group-item.active').attr('id'));
            try {
                contexto.controlador.borrarPregunta(id);
                contexto.mostrarMensaje("Exitoso", "La pregunta fue borrada correctamente", "success");
            } catch (Mensaje) {
                contexto.mostrarMensaje("Error", Mensaje, "error");
            }
        });
        e.borrarTodo.click(function() {
            contexto.controlador.borrarTodo();
            contexto.mostrarMensaje("Exitoso", "Las preguntas fueron borradas correctamente", "success");
        });
        e.botonEditarPregunta.click(function() {
            let id = parseInt($('.list-group-item.active').attr('id'));
            try {
                let Texto_Pregunta = contexto.controlador.ObtenerTextoPregunta(id);
                (async() => {
                    const { value: TextoPreguntaNuevo } = await Swal.fire({
                        title: 'Ingrese el nuevo texto para la pregunta.',
                        input: 'text',
                        inputPlaceholder: Texto_Pregunta
                    })
                    if (TextoPreguntaNuevo) {
                        contexto.controlador.editarNombrePregunta(id, TextoPreguntaNuevo);
                        contexto.mostrarMensaje("Exitoso", "El texto de la pregunta fue modificado.", "success");
                    }
                })();
            } catch (Mensaje) {
                contexto.mostrarMensaje("Error", Mensaje, "error");
            }
        });
    },

    limpiarFormulario: function() {
        $('.form-group.answer.has-feedback.has-success').remove();
    },
    mostrarMensaje: function(P_Titulo, P_Mensaje, P_Tipo_Mensaje) {
        Swal.fire({
            title: P_Titulo,
            text: P_Mensaje,
            icon: P_Tipo_Mensaje,
        })
    },
};