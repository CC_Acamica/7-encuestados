/*
 * Vista Resultados
 */
var VistaResultados = function(modelo, controlador, elementos) {
    this.modelo = modelo;
    this.controlador = controlador;
    this.elementos = elementos;
    var contexto = this;
    //suscripcion a eventos del modelo
    this.modelo.respuestaAgregada.suscribir(function() {
        contexto.reconstruirGrafico();
    });
    this.modelo.preguntaModificada.suscribir(function() {
        contexto.reconstruirGrafico();
    })
};

VistaResultados.prototype = {
    //muestra la lista por pantalla y agrega el manejo del boton agregar
    inicializar: function() {
        this.reconstruirGrafico();
    },

    //reconstruccion de los graficos de torta
    reconstruirGrafico: function() {
        var contexto = this;
        //obtiene las preguntas del local storage
        var preguntas = this.modelo.preguntas;
        preguntas.forEach(function(clave) {
            var listaParaGrafico = [
                [clave.textoPregunta, 'Cantidad']
            ];
            var respuestas = clave.cantidadPorRespuesta;
            respuestas.forEach(function(elemento) {
                listaParaGrafico.push([elemento.textoRespuesta, elemento.cantidad]);
            });
            try {
                contexto.dibujarGrafico(clave.textoPregunta, listaParaGrafico);
            } catch (Mensaje) {
                contexto.mostrarMensaje("Error", Mensaje, "error");
            }

        })
    },

    dibujarGrafico: function(nombre, respuestas) {
        var seVotoAlgunaVez = false;
        for (var i = 1; i < respuestas.length; ++i) {
            if (respuestas[i][1] > 0) {
                seVotoAlgunaVez = true;
            }
        }
        var contexto = this;
        google.charts.load("current", { packages: ["corechart"] });
        google.charts.setOnLoadCallback(drawChart);

        function drawChart() {
            var data = google.visualization.arrayToDataTable(respuestas);

            var options = {
                title: nombre,
                is3D: true,
            };
            var ubicacionGraficos = contexto.elementos.graficosDeTorta;
            var id = (nombre.replace(/\W/g, '')).split(' ').join('') + '_grafico';
            if ($('#' + id).length) { $('#' + id).remove() }
            var div = document.createElement('div');
            ubicacionGraficos.append(div);
            div.id = id;
            div.style.width = '400';
            div.style.height = '300px';
            var chart = new google.visualization.PieChart(div);
            if (seVotoAlgunaVez) {
                chart.draw(data, options);
            }
        }
    },
    mostrarMensaje: function(P_Titulo, P_Mensaje, P_Tipo_Mensaje) {
        Swal.fire({
            title: P_Titulo,
            text: P_Mensaje,
            icon: P_Tipo_Mensaje,
        })
    },
};