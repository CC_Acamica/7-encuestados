/*
 * Vista usuario
 */
var VistaUsuario = function(modelo, controlador, elementos) {
    this.modelo = modelo;
    this.controlador = controlador;
    this.elementos = elementos;
    var contexto = this;

    //suscripcion a eventos del modelo
    this.modelo.preguntaAgregada.suscribir(function() {
        contexto.reconstruirLista();
    });
    this.modelo.preguntaBorrada.suscribir(function() {
        contexto.reconstruirLista();
    });
    this.modelo.preguntaModificada.suscribir(function() {
        contexto.reconstruirLista();
    })
};

VistaUsuario.prototype = {
    //muestra la lista por pantalla y agrega el manejo del boton agregar
    inicializar: function() {
        this.reconstruirLista();
        var elementos = this.elementos;
        var contexto = this;

        elementos.botonAgregar.click(function() {
            contexto.agregarVotos();
        });
    },

    reconstruirLista: function() {
        var listaPreguntas = this.elementos.listaPreguntas;
        listaPreguntas.html('');
        var contexto = this;
        var preguntas = this.modelo.preguntas;
        preguntas.forEach(function(clave) {
            //completar
            listaPreguntas.append($('<div>', {
                id: clave.id,
                value: clave.textoPregunta,
                text: clave.textoPregunta,
            }));
            //agregar a listaPreguntas un elemento div con valor "clave.textoPregunta", texto "clave.textoPregunta", id "clave.id"
            var respuestas = clave.cantidadPorRespuesta;
            contexto.mostrarRespuestas(listaPreguntas, respuestas, clave);
        })
    },

    //muestra respuestas
    mostrarRespuestas: function(listaPreguntas, respuestas, clave) {
        respuestas.forEach(function(elemento) {
            listaPreguntas.append($('<input>', {
                type: 'radio',
                value: elemento.textoRespuesta,
                name: clave.id,
            }));
            listaPreguntas.append($("<label>", {
                for: elemento.textoRespuesta,
                text: elemento.textoRespuesta
            }));
        });
    },

    agregarVotos: function() {
        var contexto = this;
        try {
            var NombreUsuario = $('#nombreUsuario')[0].value;
            $('#preguntas').find('div').each(function() {
                var nombrePregunta = $(this).attr('value');
                var id = $(this).attr('id');
                var respuestaSeleccionada = $('input[name=' + id + ']:checked').val();
                contexto.controlador.agregarVoto(id, respuestaSeleccionada, NombreUsuario);
                $('input[name=' + id + ']').prop('checked', false);
            });
            contexto.mostrarMensaje("Exitoso", "El voto fue registrado correctamente.", "success");
        } catch (Mensaje) {
            contexto.mostrarMensaje("Error", Mensaje, "error");
        }
    },

    mostrarMensaje: function(P_Titulo, P_Mensaje, P_Tipo_Mensaje) {
        Swal.fire({
            title: P_Titulo,
            text: P_Mensaje,
            icon: P_Tipo_Mensaje,
        })
    },
};