/*
 * Modelo
 */
var Modelo = function() {
    this.preguntas = [];
    this.ultimoId = 0;

    //inicializacion de eventos
    this.preguntaAgregada = new Evento(this);
    this.preguntaBorrada = new Evento(this);
    this.preguntaModificada = new Evento(this);
    this.respuestaAgregada = new Evento(this);
    this.recuperarPreguntas();
    this.recuperarultimoID();
};

Modelo.prototype = {
    //se obtiene el id más grande asignado a una pregunta
    obtenerNuevoId: function() {
        let id = this.obtenerUltimoId();
        return this.actualizarUltimoId(id);
    },
    obtenerUltimoId: function() {
        return this.ultimoId;
    },
    actualizarUltimoId: function(id) {
        id++
        this.ultimoId = id;
        this.guardarultimoID();
        return this.ultimoId;
    },

    //se agrega una pregunta dado un nombre y sus respuestas
    agregarPregunta: function(nombre, respuestas) {
        let id = this.obtenerNuevoId();
        var nuevaPregunta = { 'textoPregunta': nombre, 'id': id, 'cantidadPorRespuesta': respuestas };
        this.preguntas.push(nuevaPregunta);
        this.guardarPreguntas();
        this.preguntaAgregada.notificar();
    },

    //se borra una pregunta dado un id
    borrarPregunta: function(id) {
        var _index = 0;
        this.preguntas.every(function(pregunta, index) {
            if (pregunta.id == id) {
                _index = index;
                return false;
            } else {
                return true;
            }
        });
        this.preguntas.splice(_index, 1);
        this.guardarPreguntas();
        this.preguntaBorrada.notificar();
    },

    //se borran todas las preguntas
    borrarTodo: function(id) {
        this.preguntas = [];
        this.guardarPreguntas();
        this.actualizarUltimoId(-1);
        this.preguntaBorrada.notificar();
    },

    //se obtiene el texto de la pregunta dado un id
    ObtenerTextoPregunta: function(id) {
        var _index = 0;
        this.preguntas.every(function(pregunta, index) {
            if (pregunta.id == id) {
                _index = index;
                return false;
            } else {
                return true;
            }
        });
        return this.preguntas[_index].textoPregunta;
    },

    //se borra una pregunta dado un id
    editarNombrePregunta: function(id, textoPregunta) {
        var _index = 0;
        this.preguntas.every(function(pregunta, index) {
            if (pregunta.id == id) {
                _index = index;
                return false;
            } else {
                return true;
            }
        });
        this.preguntas[_index].textoPregunta = textoPregunta;
        this.guardarPreguntas();
        this.preguntaModificada.notificar();
    },

    //se guardan las preguntas
    guardarPreguntas: function() {
        localStorage.removeItem("Preguntas");
        localStorage.setItem("Preguntas", JSON.stringify(this.preguntas));
    },

    //se guarda el ultimoID 
    guardarultimoID: function() {
        localStorage.removeItem("ultimoId");
        localStorage.setItem("ultimoId", this.ultimoId);
    },

    //Se recuperan las preguntas del localstorage
    recuperarPreguntas: function() {
        this.preguntas = JSON.parse(localStorage.getItem("Preguntas"));
    },

    //Se recupera el ultimoID
    recuperarultimoID: function() {
        this.ultimoId = localStorage.getItem("ultimoId");
    },

    //Se agrega una respuesta
    sumarVotoRespuesta: function(idpregunta, idrespuesta, NombreUsuario) {
        var _index_pregunta = this.BuscarIndexPregunta(idpregunta);
        var _index_respuesta = this.BuscarIndexRespuesta(_index_pregunta, idrespuesta);
        this.SumarVoto(_index_pregunta, _index_respuesta, NombreUsuario);
        this.guardarPreguntas();
        this.respuestaAgregada.notificar();
    },
    //Buscar indice correspondiente a una pregunta usando su ID
    BuscarIndexPregunta: function(idpregunta) {
        var _index_pregunta = 0;
        this.preguntas.every(function(pregunta, index) {
            if (pregunta.id == idpregunta) {
                _index_pregunta = index;
                return false;
            } else {
                return true;
            }
        });
        return _index_pregunta;
    },
    //Buscar indice correspondiente a una respuesta usando su ID y el ID de pregunta
    BuscarIndexRespuesta: function(indexpregunta, idrespuesta) {
        var _index_respuesta = 0;
        this.preguntas[indexpregunta].cantidadPorRespuesta.every(function(respuesta, index) {
            if (respuesta.textoRespuesta == idrespuesta) {
                _index_respuesta = index;
                return false;
            } else {
                return true;
            }
        });
        return _index_respuesta;
    },
    //Sumar Voto al array correspondiente
    SumarVoto: function(indexpregunta, indexrespuesta, nombreusuario) {
        this.preguntas[indexpregunta].cantidadPorRespuesta[indexrespuesta].cantidad++;
        this.preguntas[indexpregunta].cantidadPorRespuesta[indexrespuesta].votantes.push(nombreusuario);
    },
    //Validar si el id de una pregunta existe
    validarIdPregunta: function(idpregunta) {
        let valid_id_pregunta = false;
        this.preguntas.every(function(pregunta) {
            if (pregunta.id == idpregunta) {
                valid_id_pregunta = true;
                return false;
            } else {
                return true;
            }
        });
        return valid_id_pregunta;
    }
};