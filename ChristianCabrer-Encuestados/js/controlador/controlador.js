/*
 * Controlador
 */
var Controlador = function(modelo) {
    this.modelo = modelo;
};

Controlador.prototype = {
    agregarPregunta: function(pregunta, respuestas) {
        if (pregunta == "") {
            throw ("Debe completar el campo pregunta.");
        };
        if (respuestas.length < 1) {
            throw ("Debe agregar por lo menos una respuesta.");
        };
        this.modelo.agregarPregunta(pregunta, respuestas);
    },
    borrarPregunta: function(idPregunta) {
        if (isNaN(idPregunta)) {
            throw ("Debe seleccionar una pregunta.");
        };
        if (this.modelo.validarIdPregunta(idPregunta)) {
            this.modelo.borrarPregunta(idPregunta);
        } else {
            throw ("El ID de pregunta ingresado no existe.");
        };
    },
    borrarTodo: function() {
        this.modelo.borrarTodo();
    },
    editarNombrePregunta: function(idPregunta, nombre) {
        if (isNaN(idPregunta)) {
            throw ("Debe seleccionar una pregunta.");
        };
        if (nombre == "") {
            throw ("Debe completar el campo pregunta.");
        };
        if (this.modelo.validarIdPregunta(idPregunta)) {
            this.modelo.editarNombrePregunta(idPregunta, nombre);
        } else {
            throw ("El ID de pregunta ingresado no existe.");
        };
    },
    agregarVoto: function(idpregunta, idrespuesta, NombreUsuario) {
        if (!this.modelo.validarIdPregunta(idpregunta)) {
            throw ("El ID de pregunta ingresado no existe.");
        }
        if (NombreUsuario == "") {
            throw ("Debe ingresar un nombre de usuario");
        }
        if (idrespuesta != undefined) {
            this.modelo.sumarVotoRespuesta(idpregunta, idrespuesta, NombreUsuario);
        }
    },
    ObtenerTextoPregunta: function(idPregunta) {
        if (isNaN(idPregunta)) {
            throw ("Debe seleccionar una pregunta.");
        };
        if (this.modelo.validarIdPregunta(idPregunta)) {
            return this.modelo.ObtenerTextoPregunta(idPregunta);
        } else {
            throw ("El ID de pregunta ingresado no existe.");
        };
    },
};